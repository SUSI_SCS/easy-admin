package com.mars.common.response.sys;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

/**
 * 省市区VO
 *
 * @author 源码字节-程序员Mars
 */
@Data
public class SysRegionTreeResponse {

    @ApiModelProperty(value = "ID")
    private Long id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "子节点")
    private List<SysRegionTreeResponse> children;


}
