package com.mars.common.util;

import java.util.Random;

/**
 * 随机数工具类
 *
 * @author 源码字节-程序员Mars
 */
public class RandomUtils {

    /**
     * 随机生成指定长度数字
     *
     * @param length
     * @return
     */
    public static String getNumber(int length) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            sb.append(new Random().nextInt(10));
        }
        return sb.toString();
    }

    /**
     * 随机生成指定长度字母
     *
     * @param length
     * @return
     */
    public static String getLetter(int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            int choice = random.nextInt(2) % 2 == 0 ? 65 : 97;
            sb.append((char) (random.nextInt(26) + choice));
        }
        return sb.toString();
    }

    /**
     * 随机生成指定长度的字母+数字
     *
     * @param length
     * @return
     */
    public static String get(int length) {
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            String charOrNum = random.nextInt(2) % 2 == 0 ? "char" : "num";
            if ("char".equals(charOrNum)) {
                int choice = random.nextInt(2) % 2 == 0 ? 65 : 97;
                sb.append((char) (random.nextInt(26) + choice));
            } else {
                sb.append(random.nextInt(10));
            }
        }
        return sb.toString();
    }
}
