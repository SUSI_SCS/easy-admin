package com.mars.module.workflow.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.mars.module.system.entity.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 请假业务对象 biz_leave
 *
 * @author Xianlu Tech
 * @date 2019-10-11
 */
@Data
public class BizLeave extends BaseEntity {

    /**
     * 主键ID
     */
    @TableId(type = IdType.AUTO)
    private Long id;

    /**
     * 请假类型
     */
    @Excel(name = "请假类型")
    private String type;

    /**
     * 标题
     */
    @Excel(name = "标题")
    private String title;

    /**
     * 原因
     */
    @Excel(name = "原因")
    private String reason;

    /**
     * 开始时间
     */
    @Excel(name = "开始时间", width = 30, format = "yyyy-MM-dd")
    private Date startTime;

    /**
     * 结束时间
     */
    @Excel(name = "结束时间", width = 30, format = "yyyy-MM-dd")
    private Date endTime;

    /**
     * 请假时长，单位秒
     */
    @Excel(name = "请假时长，单位秒")
    private Long totalTime;

    /**
     * 流程实例ID
     */
    @Excel(name = "流程实例ID")
    private String instanceId;

    /**
     * 申请人
     */
    @Excel(name = "申请人")
    private String applyUser;

    /**
     * 申请时间
     */
    @Excel(name = "申请时间", width = 30, format = "yyyy-MM-dd")
    private Date applyTime;

    /**
     * 实际开始时间
     */
    private Date realityStartTime;

    /**
     * 实际结束时间
     */
    private Date realityEndTime;

}
