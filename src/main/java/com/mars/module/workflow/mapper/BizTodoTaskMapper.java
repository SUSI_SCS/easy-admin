package com.mars.module.workflow.mapper;


import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.workflow.entity.BizTodoTask;

/**
 * 待办事项Mapper接口
 *
 * @author 程序员Mars
 * @date 2023-11-08
 */
public interface BizTodoTaskMapper extends BasePlusMapper<BizTodoTask> {

}
