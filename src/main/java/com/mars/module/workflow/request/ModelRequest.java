package com.mars.module.workflow.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2024-01-19 14:57:33
 */
@Data
public class ModelRequest {

    @ApiModelProperty(value = "模型key")
    private String key;

    @ApiModelProperty(value = "模型名称")
    private String name;

    @ApiModelProperty(value = "模型描述")
    private String description;

}
