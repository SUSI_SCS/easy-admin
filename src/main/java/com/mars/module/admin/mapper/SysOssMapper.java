package com.mars.module.admin.mapper;

import com.mars.module.admin.entity.SysOss;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 文件存储Mapper接口
 *
 * @author mars
 * @date 2023-11-20
 */
public interface SysOssMapper extends BasePlusMapper<SysOss> {

}
