/**
 * 岗位分页列表
 * @param query
 * @returns {*}
 */
function pageList(query) {
    return requests({
        url: '/sys/post/pageList',
        method: 'post',
        data: query
    })
}


/**
 * 添加岗位
 * @param query
 * @returns {*}
 */
function add(query) {
    return requests({
        url: '/sys/post/add',
        method: 'post',
        data: query
    })
}


/**
 * 更新岗位
 * @param query
 * @returns {*}
 */
function update(query) {
    return requests({
        url: '/sys/post/update',
        method: 'post',
        data: query
    })
}

/**
 * 详情接口
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/sys/post/get/' + id,
        method: 'get'
    })
}

/**
 * 详情接口
 * @returns {*}
 */
function getList() {
    return requests({
        url: '/sys/post/list/',
        method: 'get'
    })
}
